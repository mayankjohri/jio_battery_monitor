from html.parser import HTMLParser
import urllib.request as urllib2


class MyHTMLParser(HTMLParser):
  flg = False

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):
    self.close()

  # HTML Parser Methods

  def handle_starttag(self, startTag, attrs):
    # print(startTag, attrs)
    if startTag != "label":
      return
    for key, val in attrs:
      if key == 'id' and val == "lDashBatteryQuantity":
        # print(startTag, attrs)
        self.flg = True
        break
    else:
      return

  def handle_data(self, data):
    if self.flg:
      self.flg = False
      self.data = data[:-1]


def get_battery_status():
  _url = "http://jiofi.local.html/cgi-bin/en-jio/mStatus.html"
  with MyHTMLParser() as parser:
    try:
      html_page = urllib2.urlopen(_url)
    except Exception as err:
      return None, err
    parser.feed(str(html_page.read()))
    return int(parser.data), None


if __name__ == "__main__":
  data, err = get_battery_status()
  print(data, err)
