#!/usr/bin/env pythonw
import wx
import wx.adv
from jio_status import get_battery_status
from apscheduler.schedulers.background import BackgroundScheduler
import time
import apscheduler
import os


job_defaults = {
    'coalesce': False,
    'max_instances': 3,
    'misfire_grace_time': 30
}

TRAY_TOOLTIP = 'Jiofi Battery Status'
TRAY_ICON = os.path.join("icons", '100.ico')


def create_menu_item(menu, label, func):
  item = wx.MenuItem(menu, -1, label)
  menu.Bind(wx.EVT_MENU, func, id=item.GetId())
  menu.Append(item)
  return item


class TaskBarIcon(wx.adv.TaskBarIcon):
  def __init__(self, frame):
    wx.adv.TaskBarIcon.__init__(self)
    self.myapp_frame = frame
    self.set_icon(TRAY_ICON)
    self.Bind(wx.adv.EVT_TASKBAR_LEFT_DOWN, self.on_left_down)

  def CreatePopupMenu(self):
    menu = wx.Menu()
    create_menu_item(menu, 'Check Status', self.on_check_status)
    menu.AppendSeparator()
    create_menu_item(menu, 'Exit', self.on_exit)
    return menu

  def set_icon(self, path, tooltip="TRAY_TOOLTIP"):
    icon = wx.Icon(wx.Bitmap(path))
    self.SetIcon(icon, tooltip)

  def on_left_down(self, event):
    print('Tray icon was left-clicked.')

  def on_check_status(self, event):
    self.on_battery_full()

  def on_battery_full(self):
    self.set_icon("battery-charged-icon.png")
    print('Battery is full and please unplugin it')

  def on_exit(self, event):
    self.myapp_frame.Close()


class My_Application(wx.Frame):

  # ----------------------------------------------------------------------
  def __init__(self):
    wx.Frame.__init__(self, None, wx.ID_ANY, "", size=(1, 1))
    panel = wx.Panel(self)
    self.myapp = TaskBarIcon(self)
    self.Bind(wx.EVT_CLOSE, self.onClose)

  def check_status(self):
    data, err = get_battery_status()
    msg = f"{data}% charged"
    # print(msg)
    if not err:
      if data < 25:
        icon = "0.ico"
      elif data < 50:
        icon = "25.ico"
      elif data < 75:
        icon = "50.ico"
      elif data < 100:
        icon = "75.ico"
      else:
        icon = "100.ico"
        msg += ", please unplug from charger"
      self.myapp.set_icon(os.path.join("icons", icon), msg)
    # ----------------------------------------------------------------------

  def onClose(self, evt):
    """
    Destroy the taskbar icon and the frame
    """
    self.myapp.RemoveIcon()
    self.myapp.Destroy()
    self.Destroy()


if __name__ == "__main__":
  scheduler = BackgroundScheduler(job_defaults=job_defaults)
  MyApp = wx.App()
  app = My_Application()
  app.check_status()
  scheduler.add_job(app.check_status, 'interval', seconds=120)
  scheduler.start()

  MyApp.MainLoop()
