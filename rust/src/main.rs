use glib::timeout_add_seconds;
use glib::Continue;
use gtk;
use scraper::Html;
use scraper::Selector;
use std::thread;
use std::time::Duration;
use tray_item::TrayItem;

fn get_battery_status() -> Result<String, minreq::Error> {
    let _url = "http://jiofi.local.html/cgi-bin/en-jio/mStatus.html";
    let response = minreq::get(_url).send()?;
    let resp = response.as_str()?;
    // println!("{}", resp);

    let fragment = Html::parse_fragment(resp);
    let selector = Selector::parse(r#"label[id="lDashBatteryQuantity"]"#).unwrap();

    let _input = fragment.select(&selector).next().unwrap();
    let mut val = format!("{}", _input.text().collect::<Vec<_>>()[0]);
    let len = val.len() - 1;
    val = (&val[0..len]).to_string();
    Ok(val)
}
fn main() {
    gtk::init().unwrap();

    let mut tray = TrayItem::new("JioFi3 Battery Status", "accessories-calculator").unwrap();
    let exe_path = std::env::current_exe().unwrap();
    // let _path = exe_path.parent().unwrap();
    // tray.add_label("Tray Label").unwrap();

    // tray.add_menu_item("Hello", || {
    //     println!("Hello!");
    // })
    // .unwrap();

    tray.add_menu_item("Quit", || {
        gtk::main_quit();
    })
    .unwrap();
    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    timeout_add_seconds(5, move || {
        thread::sleep(Duration::from_millis(50));
        if let Ok(battery_charge) = get_battery_status() {
            tx.send(format!("{}", battery_charge))
                .expect("Couldn't send data to channel");
        } else {
        }

        Continue(true)
    });
    rx.attach(None, move |_text| {
        let val: i32 = _text.parse().unwrap();
        let mut _icon = "0.png";
        if val < 25 {
            _icon = "0.png";
        } else if val < 50 {
            _icon = "25.png";
        } else if val < 75 {
            _icon = "50.png";
        } else if val < 95 {
            _icon = "75.png"
        } else {
            _icon = "100.png"
        }

        let icon_folder = std::path::Path::new(exe_path.parent().unwrap())
            .join("icons")
            .join(_icon)
            .into_os_string()
            .into_string();

        tray.set_icon(&icon_folder.unwrap()).unwrap();
        glib::Continue(true)
    });
    gtk::main();
}
